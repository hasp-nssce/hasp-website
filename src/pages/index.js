import React from "react"
import { Link } from "gatsby"
import titleLogo from "../assets/img/title-logo.png"
import titleImage from "../assets/img/title-image.png"
import "../assets/css/Index.css"

import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />

    <div id="Index" className="Index">
      <div className="index-wrapper">
        <div className="title-text">
            <img className="title-text-logo" src={titleLogo} alt="title-logo" />
            <h1 className="title-text-text">Hacker Space NSSCE</h1>
            <Link to="/about" className="title-text-btn">Know More</Link>
        </div>
        <div className="title-image">
          <img className="title-image-image" src={titleImage} alt="title" />
        </div>
      </div>
      <div className="motto">
        <div>
          Hack for life.
          <br />
          Hack for change.
          <br />
          Hack for better future.
          <br />
          <br />
          <br />
          <big>Let the coding culture prosper......</big>
        </div>
        <a href="/join" className="join-now">Join Now</a>
      </div>
    </div>
  </Layout>
)

export default IndexPage
